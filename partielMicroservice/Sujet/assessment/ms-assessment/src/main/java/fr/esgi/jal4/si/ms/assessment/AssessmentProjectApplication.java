package fr.esgi.jal4.si.ms.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class AssessmentProjectApplication {
	

	@RequestMapping(method = RequestMethod.POST, value="/api/text/concat")
	public String concatenation(String s1, String s2) {
		if(s1.isEmpty()||s2.isEmpty()) {
			return "400 (BAD REQUEST)";
		}
		String res = s1+" "+s2;
		return res;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/text/last_length")
	public int longueur() {
		return 0;
	}
	
	
	public static void main(String[] args) {
		SpringApplication.run(AssessmentProjectApplication.class, args);
	}

}
