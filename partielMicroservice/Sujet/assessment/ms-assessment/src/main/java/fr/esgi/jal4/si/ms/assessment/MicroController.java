package fr.esgi.jal4.si.ms.assessment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MicroController{

	@RequestMapping(method = RequestMethod.POST, value="/api/text/concat")
	public String concatenation(String s1, String s2) {
		if(s1.isEmpty()||s2.isEmpty()) {
			return "400 (BAD REQUEST)";
		}
		String res = s1+" "+s2;
		return res;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/text/last_length")
	public int longueur() {
		return 0;
	}
	
}
